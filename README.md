# README #

Projeto de Graduação de Bruno Borlini Duarte: _Aplicação do Método FrameWeb no Desenvolvimento de um Sistema de Informação na Plataforma Java EE 7_.

### Resumo ###

Inicialmente, a construção de aplicações Web se dava de forma ad-hoc. Entretanto, com o crescimento do número e da complexidade das Webapps (aplicações para a Web), foi necessário aplicar à construção deste tipo de aplicação os métodos da Engenharia de Software, adaptados a esta nova plataforma de implementação. Nasceria, assim, a Engenharia Web.

Nesse contexto, e com o objetivo de propor uma abordagem de construção de sistemas para Web, nasce então o método FrameWeb. A metodologia de desenvolvimento proposta pelo FrameWeb sugere a utilização de vários frameworks, visando a construção rápida e eficiente de sistemas de informação para a Web através de uma arquitetura básica para o desenvolvimento de uma Webapp e um perfil UML para quatro tipos de modelos de projeto que trazem conceitos utilizados por algumas categorias de frameworks.

A proposta de FrameWeb considera algumas categorias de frameworks. Dentro dessas categorias existem diversos frameworks que podem ser utilizados no desenvolvimento de um software. Esse trabalho aplica o método FrameWeb utilizando os frameworks da especificação Java EE 7 e propõe melhorias nos modelos propostos pelo método, quando necessário.
