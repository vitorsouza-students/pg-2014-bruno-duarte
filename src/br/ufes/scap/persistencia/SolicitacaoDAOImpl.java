package br.ufes.scap.persistencia;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.QueryTimeoutException;

import br.ufes.scap.dominio.Solicitacao;

@Stateless
public class SolicitacaoDAOImpl extends BaseDAOImpl<Solicitacao> implements SolicitacaoDAO {

	// private static final long serialVersionUID = 1L;

	@PersistenceContext
	private EntityManager em;

	protected EntityManager getEntityManager() {
		return em;
	}

	public Solicitacao findBySolicitante(String solicitante) {
		Solicitacao solicitacao = null;

		try {
			Query q = this.em.createNativeQuery("SELECT * FROM solicitacao,pessoa WHERE pessoa.cd_pessoa = solicitacao.cd_pessoa AND pessoa.nome like %" + solicitante + "%;");
			List result =  q.getResultList();
			solicitacao = (Solicitacao)result.get(0);
		}
		catch (NoResultException e1) {
			System.out.println("Nenhuma solicita��o encontrada");
		}
		catch (NonUniqueResultException e2) {
			System.out.println("Mais de uma pessoa cadastada com a mesma matricula");
		}
		catch (QueryTimeoutException e3) {
			System.out.println("Query timmed out");
		}
		catch (Exception e4) {
			e4.printStackTrace();
		}
		return solicitacao;
	}

	public void persistirSolicitacao(Solicitacao solicitacaoAfast) {
		em.getTransaction().begin();
		// entityManager.persist(solicitacaoAfast);
		em.merge(solicitacaoAfast);
		em.getTransaction().commit();
	}

	@Override
	public Solicitacao retrieveByPK(int pk) {
		Solicitacao solicitacao = new Solicitacao();

		solicitacao = em.find(Solicitacao.class, pk);

		return solicitacao;
	}
}
