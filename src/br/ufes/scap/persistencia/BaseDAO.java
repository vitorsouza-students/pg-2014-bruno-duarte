package br.ufes.scap.persistencia;

public interface BaseDAO<T> {

	// public EntityManager getConnection();

	public void salvar(T object);

	T merge(T object);

}
