package br.ufes.scap.aplicacao;

import javax.ejb.EJB;
import javax.ejb.Stateless;
//import javax.enterprise.context.SessionScoped;

import br.ufes.scap.dominio.Parecer;
import br.ufes.scap.persistencia.ParecerDAO;

@Stateless
public class ParecerService {

	@EJB
	private ParecerDAO parecerDAO;

	public void SalvarParecer(Parecer parecer) {

		parecerDAO.salvar(parecer);

	}
}
